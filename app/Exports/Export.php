<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;


class Export implements FromArray
{

    public function array(): array
    {
        $users = User::with('address:id,name,neighborhood,cep,city_id','address.city','address.city.state')->select('name','document','telephone','address_id')->first()->get();

        $list = [];

        $list[-1]["nome"]      = "nome";
        $list[-1]["documento"] = "documento";
        $list[-1]["cep"]       = "cep";
        $list[-1]["endereco"]  = "endereco";
        $list[-1]["bairro"]    = "bairro";
        $list[-1]["cidade"]    = "cidade";
        $list[-1]["uf"]        = "uf";
        $list[-1]["telefone"]  = "telefone";
        $list[-1]["email"]     = "email";
        $list[-1]["ativo"]     = "ativo";

        foreach ($users as $key => $user){

            $list[$key]["nome"]      = $user->name;
            $list[$key]["documento"] = $user->document;
            $list[$key]["cep"]       = $user->address->cep;
            $list[$key]["endereco"]  = $user->address->name;
            $list[$key]["bairro"]    = $user->address->neighborhood;
            $list[$key]["cidade"]    = $user->address->city->name;
            $list[$key]["uf"]        = $user->address->city->state->name;
            $list[$key]["telefone"]  = $user->telephone;
            $list[$key]["email"]     = $user->email;

            if($user->active == 1){
                $list[$key]["ativo"] = "SIM";
            }else{
                $list[$key]["ativo"] = "Não";
            }
        }

        return $list;
    }
}