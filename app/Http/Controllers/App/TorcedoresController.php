<?php

namespace App\Http\Controllers\App;

use App\Models\Address;
use App\Models\City;
use App\Models\Estate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Response;
use Session;

class TorcedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = [];
        $list = DB::table('users')->select('id','name','email', 'document','telephone','active')->orderBy('name', 'asc')->get();

        return view('site.listUsers.listaTorcedores', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = Estate::all();
        return view('site.listUsers.createTorcedor',compact('estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->replace($request);

        $address = new Address();

        $address->name = $request->input('addressName');
        $address->neighborhood = $request->input('neighborhood');
        $address->cep = $request->input('cep');
        $address->city_id = $request->input('cidade');
        $address->save();

        $user = new User();
        $user->name = $request->input('name');
        $user->document = $request->input('document');
        $user->email = $request->input('email');
        $user->telephone = $request->input('telephone');
        $user->active = $request->input('active');
        $user->address_id = $address->id;
        $user->save();


        Session::flash('msg', 'Usuario criado com sucesso');
        $url = "torcedor/$user->id";
        return redirect($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('address:id,name,neighborhood,cep,city_id','address.city','address.city.state')->find($id);

        $estados = Estate::all();
        $cidades = City::all();

        return view('site.listUsers.showTorcedor', compact('user','estados','cidades'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request = $this->replace($request);

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->document = $request->input('document');
        $user->email = $request->input('email');
        $user->telephone = $request->input('telephone');
        $user->active = $request->input('active');
        $user->save();

        $address = Address::find($user->address_id);

        $address->name = $request->input('addressName');
        $address->neighborhood = $request->input('neighborhood');
        $address->cep = $request->input('cep');
        $address->city_id = $request->input('cidade');
        $address->save();

        Session::flash('msg', 'Usuario atualizado com sucesso.');
        $url = "torcedor/$user->id";
        return redirect($url);
    }


    public function getCidades($idEstado)
    {

        $estados = Estate::find($idEstado);
        $cidades = $estados->city()->getQuery()->get(['id', 'name']);
        foreach ($cidades as $key => $state){
            $cidades[$key]['id'] = $state->id;
            $cidades[$key]['name'] = mb_convert_case($state->name, MB_CASE_TITLE, "UTF-8");
        }

        return Response::json($cidades);
    }

    public function replace($request)
    {
        $documento = $request->input('document');
        $telephone = $request->input('telephone');
        $cep = $request->input('cep');

        $documento = str_replace(".", "", $documento);
        $documento = str_replace("-", "", $documento);

        $telephone = str_replace("(", "", $telephone);
        $telephone = str_replace(")", "", $telephone);
        $telephone = str_replace(" ", "", $telephone);
        $telephone = str_replace("-", "", $telephone);

        $cep = str_replace("-", "", $cep);

        $request->merge(['document' => $documento]);
        $request->merge(['telephone' => $telephone]);
        $request->merge(['cep' => $cep]);

        return $request;
    }
}
