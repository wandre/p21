<?php

namespace App\Http\Controllers\ImportExport;

use App\Exports\Export;
use App\Models\Address;
use App\Models\City;
use App\Models\Estate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Imports\UsersImport;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Concerns\ToArray;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;


class ImportTorcedoresController extends Controller
{


    public function index(){

        return view('site.import.index');
    }


    public function import(Request $request)
    {
        // Faz verificações para saber se está no formato correto.
        $list = $this->verifications($request);

        if($list){
            foreach($list as $row){
                //Limpa os campos
                $row = $this->replace($row);

                $this->processEstate($row);
            }

            Session::flash('msg', 'Seu arquivo foi importado e os dados atualizados com sucesso.');
            return redirect('/importar-torcedores');
        }

        Session::flash('msgErro', 'Arquivo não compativel. Faça upload somente de arquivos com a extensão xml ou xlsx');
        return redirect('/importar-torcedores');
    }

    public function processEstate($row)
    {
        $estate = Estate::query()
            ->where('name', $row['uf'])
            ->get()
            ->first();

        if (!$estate) {
            $estate = Estate::create([
                'name' => $row['uf']
            ]);
        }

        $this->processCity($row,$estate);
    }


    public function processCity($row,$estate)
    {
        $city = City::query()
            ->where('name', $row['cidade'])
            ->get()
            ->first();

        if (!$city) {
            $city = City::create([
                'name'      => $row['cidade'],
                'estate_id' => $estate->id
            ]);
        }

        $this->processAddress($row,$city);
    }

    public function processAddress($row,$city)
    {
        $address = Address::query()
            ->where('name', $row['endereco'])
            ->get()
            ->first();

        if (!$address) {
            $address = Address::create([
                'name'         => $row['endereco'],
                'neighborhood' => $row['bairro'],
                'cep'          => $row['cep'],
                'city_id'      => $city->id
            ]);
        }
        else{

            // Atualiza os dados existentes.
            $address->name         = $row['endereco'];
            $address->neighborhood = $row['bairro'];
            $address->cep          = $row['cep'];
            $address->save();
        }

        $this->processUser($row,$address);
    }

    public function processUser($row,$address)
    {
        $user = User::query()
            ->where('document', $row['documento'])
            ->get()
            ->first();

        if (!$user) {
            $user = User::create([
                'name'       => $row['nome'],
                'email'      => $row['email'],
                'document'   => $row['documento'],
                'telephone'  => $row['telefone'],
                'active'     => $row['ativo'],
                'address_id' => $address->id
            ]);

        }else{

            // Atualiza os dados existentes.
            $user->name      = $row['nome'];
            $user->email     = $row['email'];
            $user->telephone = $row['telefone'];
            $user->active    = $row['ativo'];
            $user->save();
        }

        return $user;
    }


    public function replace($row){
        $row['documento'] = str_replace(".", "", $row['documento']);
        $row['documento'] = str_replace("-", "", $row['documento']);

        $row['telefone'] = str_replace("(", "", $row['telefone']);
        $row['telefone'] = str_replace(")", "", $row['telefone']);
        $row['telefone'] = str_replace(" ", "", $row['telefone']);
        $row['telefone'] = str_replace("-", "", $row['telefone']);

        $row['cep'] = str_replace("-", "", $row['cep']);

        //Coloca um nome caso o campo venha vazio.
        $row['nome']       = !empty($row['nome'])      ? $row['nome']      : "Sem nome";
        $row['documento']  = !empty($row['documento']) ? $row['documento'] : "000000000000";
        $row['cep']        = !empty($row['cep'])       ? $row['cep']       : "000000000";
        $row['endereco']   = !empty($row['endereco'])  ? $row['endereco']  : "Sem endereço";
        $row['bairro']     = !empty($row['bairro'])    ? $row['bairro']    : "Sem bairro";
        $row['telefone']   = !empty($row['telefone'])  ? $row['telefone']  : "00000000000";
        $row['email']      = !empty($row['email'])     ? $row['email']     : "Sem email";
        $row['ativo']      = !empty($row['ativo'])     ? $row['ativo']     : "0";
        $row['cidade']     = !empty($row['cidade'])    ? $row['cidade']    : "Sem cidade";
        $row['uf']         = !empty($row['uf'])        ? $row['uf']        : "Sem estado";


        return $row;
    }

    public function verifications($request){

        // Verifica se existe um arquivo é se ele é do tipo XML
        if($request->hasFile('import_file') && Input::file('import_file')->getClientOriginalExtension() == "xml" ) {

            //Pega o caminho real e faz a leitura do arquivo
            $path = Input::file('import_file')->getRealPath();
            $data = file_get_contents($path);
            $xml = simplexml_load_string($data);

            //Verifica se o documento é valido.
            if(isset($xml->torcedor[0])){

                $xml1 = $xml->torcedor;

                return $xml1;
            }
        }

        // Verifica se o arquivo é um xlsx
        if(Input::hasFile('import_file') && Input::file('import_file')->getClientOriginalExtension() == "xlsx" ) {

            $data = Excel::toArray(new UsersImport,Input::file('import_file'));

            // Verifica se os campos estão na ordem certa
            if($data[0][0][0] == "NOME" &&  $data[0][0][1] == "DOCUMENTO" &&  $data[0][0][9] == "ATIVO"){

                $list = [];
                // Renomeia os campos
                foreach ($data[0] as $key => $user){

                    if($key > 0){

                        $user[1] = substr($user[1], 2);

                        $list[$key]['nome']      = $user[0];
                        $list[$key]['documento'] = $user[1];
                        $list[$key]['cep']       = $user[2];
                        $list[$key]['endereco']  = $user[3];
                        $list[$key]['bairro']    = $user[4];
                        $list[$key]['cidade']    = $user[5];
                        $list[$key]['uf']        = $user[6];
                        $list[$key]['telefone']  = $user[7];
                        $list[$key]['email']     = $user[8];


                        if($user[9] == "SIM"){
                            $list[$key]['ativo'] = "1";
                        }else{
                            $list[$key]['ativo'] = "0";
                        }
                    }
                }

                //Destroi a variavel
                unset($data);

                return $list;

            }

        }

        return false;
    }


}
