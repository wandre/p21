@php
    if(!isset($user)){
        $user = null;
    }
@endphp


<div class="form-group col-xs-7">
    <label for="Nome">Nome</label>
    <input class="form-control input-sm" id="nome" type="text" name="name" minlength="4" value="@isset($user) {{$user->name }} @endisset" required autofocus>
</div>
<div class="form-group col-xs-5">
    <label for="Documento">Documento</label>
    <input class="form-control input-sm" id="documento" type="text" name="document" data-mask="000.000.000-00" minlength="14" maxlength="14" value="@isset($user){{$user->document?: ''}}@endisset" >
</div>
<div class="form-group col-xs-7">
    <label for="email">Email</label>
    <input class="form-control input-sm" id="email" type="email" name="email" value="@isset($user){{$user->email?: ''}}@endisset" required autofocus>
</div>
<div class="form-group col-xs-3">
    <label for="telefone">Telefone</label>
    <input class="form-control input-sm" id="telefone" type="text" name="telephone" data-mask="(00) 0 0000-0000" minlength="16" maxlength="16" value="@isset($user){{$user->telephone?: ''}}@endisset" required autofocus>
</div>

<div class="form-group col-xs-2">
    <label for="Ativo">Ativo</label>
    <select class="form-control input-sm"  name="active" data-mask-selectonfocus="true"  >
        <option value="0" @isset($user) @if($user->active == 0)selected @endif @endisset> Não</option>
        <option value="1" @isset($user) @if($user->active == 1)selected @endif @endisset> Sim </option>
    </select>
</div>

<div class="form-group col-xs-7">
    <label for="endereco">Endereco</label>
    <input class="form-control input-sm" id="endereco" type="text" name="addressName"  value="@isset($user){{$user->address->name?: ''}}@endisset" required autofocus >
</div>
<div class="form-group col-xs-5">
    <label for="bairro">Bairro</label>
    <input class="form-control input-sm" id="bairro" type="text" name="neighborhood"  value="@isset($user){{$user->address->neighborhood?: ''}}@endisset" required autofocus>
</div>

<div class="form-group col-xs-4">
    <label for="Estado">Estado</label>
    <select class="form-control input-sm"  name="estado" required autofocus data-mask-selectonfocus="true">
        <option value=" @isset($user) {{$user->address->city->state->id?: ''}}@endisset" selected> @isset($user) {{$user->address->city->state->name?: ''}}@endisset</option>
        @foreach($estados as $key =>$estado)
            <option value="{{$estado->id}}">{{$estado->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-xs-4">
    <label for="Cidade">Cidade</label>
    <select required class="form-control input-sm"  name="cidade"   >
        <option value=" @isset($user) {{$user->address->city->id?: ''}}@endisset" selected> @isset($user){{$user->address->city->name?: ''}}@endisset</option>
        <option> </option>
    </select>
</div>


<div class="form-group col-xs-4">
    <label for="cep">CEP</label>
    <input class="form-control input-sm" id="cep" type="text" name="cep" data-mask="00000-000" minlength="9" maxlength="9" value="@isset($user){{$user->address->cep?: ''}}@endisset" required autofocus>
</div>