@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="col-md-11">
            <h1> Lista Torcedores </h1>
            <p>Quantidade: {{count($list)}}</p>

            <a href="/torcedor-create" class="btn btn-success"style="float: left"> Adicionar</a>

            <form action="/exportar" method="get">
                <button type="submit" class="btn btn-warning" style="float: right">Exportar torcedores</button>
            </form>
            <br>
            <br>
            <br>
        </div>


        <input class="form-control" id="searchTable" type="text" placeholder="Search..">
        <br>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Documento</th>
                <th>Telefone</th>
                <th>Editar</th>
            </tr>
            </thead>
            <tbody id="tableTorcedores">
                @foreach($list as $name)
                    <tr>
                    <td>{{$name->name}}</td>
                    <td>{{$name->email}}</td>
                    <td>{{$name->document}}</td>
                    <td>{{$name->telephone}}</td>

                    <td>
                        <form action="{{ url('/torcedor',$name->id) }}" method="GET" >
                            <button type="submit" class="btn btn-success">Editar</button>
                        </form>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>


    </div>
@endsection