@extends('layouts.app')

@section('content')


    <div class="container">
        <h2>Torcedor: {{$user->name}}</h2>
        <br>

        <form action="{{ url('/torcedor-update',$user->id) }}" method="POST">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            @include("site.listUsers._include.formTorcedor")

            <button type="submit" class="btn btn-success"style="float: right">Atualizar</button>
        </form>

        <a href="/lista-torcedores" class="btn btn-danger"style="float: left"> Voltar</a>
        <br>
        <br>
        <br>
    </div>
@endsection