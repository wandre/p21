<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('/lista-torcedores');
});


Route::get('notificar-torcedores', 'App\NotificarTorcedoresController@index');
Route::post('notificar-torcedores', 'App\NotificarTorcedoresController@notificar');

Route::get('lista-torcedores', 'App\TorcedoresController@index');
Route::get('torcedor/{id}', 'App\TorcedoresController@show');
Route::post('torcedor-update/{id}', 'App\TorcedoresController@update');
Route::get('torcedor-create', 'App\TorcedoresController@create');
Route::post('torcedor-create', 'App\TorcedoresController@store');

Route::get('getCidades/{idEstado}', 'App\TorcedoresController@getCidades');

Route::get('importar-torcedores', 'ImportExport\ImportTorcedoresController@index');
Route::post('importar-usuarios', 'ImportExport\ImportTorcedoresController@import')->name('importar-usuarios');
Route::get('exportar', 'ImportExport\ExportTorcedoresController@export');

